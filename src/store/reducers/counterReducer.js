const counterReducer = (state = { counter: 0, counterActions: 0 }, action) => {
    switch (action.type) {
        case "INCREMENT":
            return {
                ...state,
                counter: state.counter + 1
            }
        case "DECREMENT":
            return {
                ...state,
                counter: state.counter - 1
            }
        case "INCREMENT_N":
            return {
                ...state,
                counter: state.counter + action.payload
            }
        default:
            return state
    }
}

export default counterReducer;