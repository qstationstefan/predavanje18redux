import { useDispatch, useSelector } from "react-redux";
import Counter from "./components/Counter";

function App() {
  const isLogged = useSelector(state => state.authReducer.isLogged);
  const dispatch = useDispatch();

  const loginHandler = () => {
    dispatch({ type: isLogged ? "SIGN_OUT" : "SIGN_IN" })
  }

  return (
    <div>
      <button onClick={loginHandler}>Log in/out</button>
      {isLogged && <Counter />}
    </div>
  );
}

export default App;
